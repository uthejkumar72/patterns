package com.flower.services;

import java.io.IOException;
import java.util.Map;

import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.AsyncHttpRequest;
import org.platformlambda.core.models.LambdaFunction;

import com.sun.media.sound.InvalidDataException;

public class BookDisplayOne implements LambdaFunction{
	@Override
    public Object handleEvent(Map<String, String> headers, Object body, int instance) throws AppException, IOException {
		

		AsyncHttpRequest request = new AsyncHttpRequest(body);
		Object result;
		BookListMap book=new BookListMap();
		
		String bookId=request.getPathParameter("bookId");
		//String bookName=request.getQueryParameter("bookname");
		//String bookAuthor=request.getQueryParameter("bookauthor");
		
		
		System.out.println("values from get path parameter --\nbookid-->"+bookId);
	    
	    if(bookId == null)
		{
			throw new IllegalArgumentException("Invalid key term used.\nKindly check the key used.\nKey term to be used is bookid");
		}
	    //System.out.println("All key terms entered correctly");
		if(bookId.isEmpty())
		{
			throw new InvalidDataException("Empty/invalid value passed in key.\nKindly validate the value passed in key.");
		}
		result = book.displayone(bookId);
		return result;
		}

}
