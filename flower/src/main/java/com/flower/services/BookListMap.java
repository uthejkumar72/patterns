package com.flower.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class BookListMap {
	
	static Map<String,List<String>> bookmap = new HashMap<>();
	String idDoesNotExist = "You Passed a value which doesn't exist.\nKindly enter the valid existing value for id (To get the id value refer the link api/bookstore/action).";
	String idAlreadyExist = "The book id value mentioned already exist.\nKindly enter a new book id.";
	List<String> values =new ArrayList<> ();
	
	public   Object addbook (String bookid,String bookname,String bookauthor)
	
	{
		if(bookmap.containsKey(bookid)){
			throw new IllegalArgumentException(idAlreadyExist);
		}	
		values.add(bookname);
		values.add(bookauthor);
		
		 bookmap.put(bookid, values);
		
		 System.out.println("values from addbook --result----->\n"+bookmap);
		return bookmap;
	}
	
	public  Object displayall()
	{
		  
		 if(bookmap.isEmpty())
		 {
			 System.out.println("values from displayall empty part------>\n"+bookmap);
			 return "No Books are added";
		 }else{
			 System.out.println("values from display--results----->\n"+bookmap);
			 return bookmap;
		 }
	}
	
	public  Object displayone(String bookid)
	{
		  
		if(bookmap.containsKey(bookid)){
			 List<String> value = bookmap.get(bookid);
			 System.out.println("values from displayone ------>\n");
			 return value;
		 }else{
			 return idDoesNotExist;
		 }
	}
	
	public  Object updatebook (String bookid,String bookname,String bookauthor)
	
	{
		
		 if(bookmap.containsKey(bookid)){
			 values.add(bookname);
			 values.add(bookauthor);
			
			 bookmap.replace(bookid, values);
			
			 System.out.println("values from updatebook --result----------------->\n"+bookmap);
			 return bookmap;
			}
		  else
			{
			 return idDoesNotExist;
			}
	}
	public  Object deletebook (String bookid)
	
	{		  
		 
		 if(bookmap.containsKey(bookid))
			{
				bookmap.remove(bookid);
				
				System.out.println("values from deletebook --results----------------->\n"+bookmap);
				return bookmap;
			}
		  else
			{
			  return idDoesNotExist;
			}
	}

}
