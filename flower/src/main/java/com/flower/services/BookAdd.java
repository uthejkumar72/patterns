package com.flower.services;

import java.io.IOException;
import java.util.Map;

import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.AsyncHttpRequest;
import org.platformlambda.core.models.LambdaFunction;

import com.sun.media.sound.InvalidDataException;

public class BookAdd implements LambdaFunction {

	@Override
	    public Object handleEvent(Map<String, String> headers, Object body, int instance) throws AppException, IOException {
		AsyncHttpRequest request = new AsyncHttpRequest(body);
		BookListMap books=new BookListMap();
	    Object result;
			String bookId=request.getQueryParameter("bookid");
		    String bookName=request.getQueryParameter("bookname");
		    String bookAuthor=request.getQueryParameter("bookauthor");
		    System.out.println("values from get query parameter in Add--\nbookid-->"+bookId+"\nbookname-->"+bookName+"\nbookauthor-->"+bookAuthor);
		    
		    if(bookId == null || bookName == null || bookAuthor == null)
			{
				throw new IllegalArgumentException("Invalid key term used.\nKindly check the key used.\nKey term to be used are:-\n1. bookid \n2.bookname\n3.bookauthor");
			}
		    //System.out.println("All key terms entered correctly");
			if(bookId.isEmpty() || bookName.isEmpty() || bookAuthor.isEmpty())
			 {
				throw new InvalidDataException("Empty/invalid value passed in key.\nKindly validate the value passed in key.");
			 }
		     result =books.addbook(bookId,bookName,bookAuthor);
		
		return result;
			 
	}

	}

