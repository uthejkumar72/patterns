package com.flower.services;

import java.io.IOException;
import java.util.Map;

import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.LambdaFunction;

public class BookDisplay implements LambdaFunction{
	
	@Override
    public Object handleEvent(Map<String, String> headers, Object body, int instance) throws AppException, IOException {
		BookListMap books=new BookListMap();
		Object results=books.displayall();
		
		System.out.println("values from hellodisplay--------"+results);
		return results;
		
	}

}
