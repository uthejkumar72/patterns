package com.flower.services;

import java.io.IOException;
import java.util.Map;

import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.AsyncHttpRequest;
import org.platformlambda.core.models.LambdaFunction;




public class BookRemove implements LambdaFunction {
	
	@Override
    public Object handleEvent(Map<String, String> headers, Object body, int instance) throws AppException, IOException {

		AsyncHttpRequest request = new AsyncHttpRequest(body);
		Object result;
		String bookId=request.getPathParameter("bookId");
		BookListMap books=new BookListMap();
		
		if(bookId ==null){
			throw new IllegalArgumentException("No key value passed in url.\nKindly check the key value used.\nKey value to be passed in url after book/ enter a valid existing id");
		}
		result=books.deletebook(bookId);
		//System.out.println("values from hellodelete---results----->\n"+result);
		return result;
		
	}
}
