package com.lotus.models;



public class BookStore {
		private Integer bookid;
		private String bookname;
		
		public BookStore(Integer bookid,String bookname) {
			 
			 this.bookid = bookid;
		     this.bookname = bookname;
		}

		public Integer getBookid() {
			return bookid;
		}

		public void setBookid(Integer bookid) {
			this.bookid = bookid;
		}

		public String getBookname() {
			return bookname;
		}

		public void setBookname(String bookname) {
			this.bookname = bookname;
		}

	public BookStore() {
			// TODO Auto-generated constructor stub
		}
			
		
		
}
