/*

    Copyright 2018-2020 Accenture Technology

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */

//package com.lotus.flower;
package org.platformlambda.example;


import com.flower.services.BookAdd;
import com.flower.services.BookDisplay;
import com.flower.services.BookDisplayOne;
import com.flower.services.BookRemove;
import com.flower.services.BookUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

import org.platformlambda.core.annotations.MainApplication;
import org.platformlambda.core.models.EntryPoint;
import org.platformlambda.core.models.LambdaFunction;
import org.platformlambda.core.system.AppStarter;
import org.platformlambda.core.system.Platform;


@MainApplication
public class MainApp implements EntryPoint {
    private static final Logger log = LoggerFactory.getLogger(MainApp.class);

    public static void main(String[] args) {
        AppStarter.main(args);
    }


    public void start(String[] args) throws Exception {
        // Start the platform
        Platform platform = Platform.getInstance();

        // You can create a microservice as a lambda function inline or write it as a regular Java class
        LambdaFunction echo = (headers, body, instance) -> {
            log.info("echo @"+instance+" received - "+headers+", "+body);
            Map<String, Object> result = new HashMap<>();
            result.put("headers", headers);
            result.put("body", body);
            result.put("instance", instance);
            result.put("origin", platform.getOrigin());
            return result;
        };

        // register your services
        platform.register("book.add", new BookAdd(), 5);
        platform.register("book.update", new BookUpdate(), 5);
        platform.register("book.remove", new BookRemove(), 5);
        platform.register("book.display", new BookDisplay(), 5);
        platform.register("book.display.one", new BookDisplayOne(), 5);
        platform.register("hello.world", echo, 10);
        // connect to cloud according to "cloud.connector" in application.properties
        platform.connectToCloud();
    }

}
