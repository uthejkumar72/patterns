package com.example.Rest;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.models.Kv;
import org.platformlambda.core.system.PostOffice;
import org.platformlambda.core.util.Utility;


@Path("/model")

public class RequestModels {
	/*****************************************************************
	 *           Request and response pattern
	 *****************************************************************/
	@GET
    @Path("/rpc/{id}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
    public Object getajay(@PathParam("id") Integer id) throws TimeoutException, AppException, IOException {

        PostOffice po = PostOffice.getInstance();
        EventEnvelope response = po.request("hello.ajay", 3000, new Kv("id", id));
        
    
            return response.getBody();
    }
	
	
	
	
	
	
	/************************************************************
	 *              Send and forgot patterns
	 *************************************************************/
	@GET
    @Path("/send/{id}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
    public Object getsend(@PathParam("id") Integer id) throws TimeoutException, AppException, IOException {

        PostOffice po = PostOffice.getInstance();
        
        String message ="The secret of joy in work is contained in one word EXCELLENCE. To know how to do something well is to enjoy it. -Pearl Buck";
        
        String Output ="po.send has been successfully executed...Have a great day";
        System.out.println("Message from HelloEmployee class-------"+message);
        
        po.send("hello.send", message, new Kv("id",id));
        
        return Output;
	}
	
	
	/***************************************************************
	 *                 Call Back Patterns					
	 ****************************************************************/
	
	
	@GET
    @Path("/callback/{id}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
    public Object getcallback(@PathParam("id") Integer id) throws TimeoutException, AppException, IOException {

        PostOffice po = PostOffice.getInstance();
       
        String happy ="hello.send";
        
        String message ="Welcome to Mercury Platform ,Have a Nice day";
        
        System.out.println("Message from HelloEmployee class-------\n"+message);
        
        
        
        po.send("hello.ajay", message, new Kv("id",id),new Kv("happy",happy));
       
            EventEnvelope event = new EventEnvelope();
            EventEnvelope responses=event.setReplyTo("hello.ajay");
        

       // Map<String, Object> results = new HashMap<>();
        
        
      
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"+responses);
        System.out.println("++++++++++++++++++++++++++++++++\n"+responses.getReplyTo());

        return responses;
  }
        
        
        
        
        
        
        
        
	
	
	/****************************************************************
	 *              Fork and join (Concurrent)
	 *****************************************************************/
	
	@GET
    @Path("/Parallel")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
    public Map<String, Object> parallel(@Context HttpServletRequest request) throws IOException, AppException {

        Utility util = Utility.getInstance();
        PostOffice po = PostOffice.getInstance();
        
        int TOTAL = 8;
        List<EventEnvelope> parallelEvents = new ArrayList<>();
        for (int i=0; i < TOTAL-3; i++) {
            EventEnvelope event = new EventEnvelope();
            event.setHeader("request no", "#"+(i+1));
            event.setHeader("id", "1");
            event.setTo("hello.ajay");
            parallelEvents.add(event);
        }
        for (int i=5; i < TOTAL; i++) {
            EventEnvelope event = new EventEnvelope();
            event.setHeader("request no", "#"+(i+1));
            event.setTo("hello.world");
            parallelEvents.add(event);
        }

        Map<String, Object> results = new HashMap<>();
        
        List<EventEnvelope> responses = po.request(parallelEvents, 3000);
        
        if (responses.size() < TOTAL) {
            throw new AppException(408, "Only "+responses.size()+" of "+TOTAL+" responded");
        }
        int n = 0;
        for (EventEnvelope evt: responses) {
            Map<String, Object> singleResult = new HashMap<>();
            singleResult.put("status", evt.getStatus());
            singleResult.put("TracePath",evt.getTracePath());
            singleResult.put("SourcePath", evt.getFrom());
            singleResult.put("headers", evt.getHeaders());
            singleResult.put("body", evt.getBody());
            singleResult.put("Destination", evt.getTo());
            singleResult.put("round_trip", evt.getRoundTrip());
            n++;
            results.put("result_"+util.zeroFill(n, 999), singleResult);
        }
        return results;
    }
        
}
