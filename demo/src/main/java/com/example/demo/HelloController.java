package com.example.demo;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
@RequestMapping("/")
		public String index() {
			return "Greetings from Spring Boot";
		}

@RequestMapping("/all")
public String index1() {
	//request("hello.pojo", 3000, new Kv("id", id));
	return "Greetings to All";
}
}
