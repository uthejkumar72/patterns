package com.example.demo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.platformlambda.core.serializers.MsgPack;
import org.platformlambda.core.serializers.PayloadMapper;

public class MsgTest {

	 private static final MsgPack msgPack = new MsgPack();
	 @Test
	 public void dataIsMap() throws IOException {
	        Map<String, Object> input = new HashMap<>();
	        input.put("hello", "world");
	        input.put("boolean", true);
	        input.put("integer", 12345);
	        input.put(PayloadMapper.NOTHING, null);
	        
	        byte[] b = msgPack.pack(input);
	        System.out.println("after unpacking"+b);
	        //Object o = msgPack.unpack(b);
	 }
}
